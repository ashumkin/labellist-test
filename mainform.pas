unit mainform;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, ll_interfaces, ll_model;

const
  TopOffset = 80;
  LeftOffset = 30;

type

  { TForm1 }

  TForm1 = class(TForm, ILabelsOwner)
    Timer1: TTimer;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    LL: ILabelsContainer;
    indexL: integer;
    procedure SetParentForm(l: TGraphicControl);
    function GetTopOffset: integer;
    function GetLeftOffset: integer;
   public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormShow(Sender: TObject);
begin
     LL := TLabelsContainer.Create(10,Self);
     indexL := 0;
     Timer1.Enabled:=True;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  if LL=nil then exit;
  LL.AddLabel('Метка номер '+IntToStr(indexL));
  Inc(indexL);
end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
     Timer1.Enabled:=False;
     LL:=nil;
end;

procedure TForm1.SetParentForm(l: TGraphicControl);
begin
     if l<>nil then begin
       l.Parent := Self;
       l.Visible := True;
     end;
end;

function TForm1.GetTopOffset: integer;
begin
     Result := Self.Height - TopOffset;
end;

function TForm1.GetLeftOffset: integer;
begin
     Result := LeftOffset;
end;

end.

