Тренировочный проект 

Контейнер для списка меток, с помощью которого можно организовать своеобразный статус-информер, например, при загрузке, при длительной работе какого-то алгоритма и т.п. Метки появляются снизу с заданным максимальным количеством строк, и постепенно поднимаются в виде очереди вверх.

Цели:
- список на дженериках TFPGObjectList
- программирование на уровне интерфейсов